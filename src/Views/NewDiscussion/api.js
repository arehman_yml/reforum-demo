import { postData } from '../../Utils/axios';

export const postDiscussionApi = (discussion) => {
  return postData('/api/discussion/newDiscussion', discussion);
};
