import React, { Component } from "react";
import { withRouter } from "react-router";
import cx from "classnames";

import styles from "./styles.module.css";
import { connect } from "react-redux";
import { signUp } from "../../App/actions";

class SignUp extends Component {
  state = {
    fullName: "",
    email: "",
    city: "",
    password: "",
    confirmPassword: "",
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.authenticated) {
      this.props.history.push("/");
    }
  }

  onSubmit = (e) => {
    e.preventDefault();
    const { confirmPassword, ...userData } = this.state;
    this.props.signUp(userData);
  };

  goToLogin = () => {
    this.props.history.push("/auth/login");
  };

  updateForm = (e) => this.setState({ [e.target.id]: e.target.value });

  render() {
    const { fullName, email, city, password, confirmPassword } = this.state;
    const activeBtnCss = cx([styles.HeaderButton, styles.active]);
    return (
      <main className={styles.signUpContainer}>
        <div className={styles.header}>
          <button className={activeBtnCss}>Sign up</button>
          <button className={styles.HeaderButton} onClick={this.goToLogin}>
            Log in
          </button>
        </div>
        <form
          name="signup-form"
          className={styles.signupForm}
          onSubmit={this.onSubmit}
        >
          <label htmlFor="fullName">Full Name</label>
          <input
            id="fullName"
            type="text"
            value={fullName}
            onChange={this.updateForm}
          />

          <label htmlFor="city">City</label>
          <input
            id="city"
            type="text"
            value={city}
            onChange={this.updateForm}
          />

          <label htmlFor="email">Email</label>
          <input
            id="email"
            type="text"
            value={email}
            onChange={this.updateForm}
          />

          <label htmlFor="password">Password</label>
          <input
            id="password"
            type="password"
            value={password}
            onChange={this.updateForm}
          />

          <label htmlFor="confirmPassword">Confirm Password</label>
          <input
            id="confirmPassword"
            type="password"
            value={confirmPassword}
            onChange={this.updateForm}
          />

          <a href="/login" className={styles.loginLink}>
            Already having an account?
          </a>
          <button type="submit" className={styles.submitBtn}>
            Create account
          </button>
        </form>
      </main>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  signUp: (data) => dispatch(signUp(data)),
});

export default withRouter(connect(null, mapDispatchToProps)(SignUp));
// export default connect(null, mapDispatchToProps)(SignUp);
