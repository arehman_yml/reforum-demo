import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";
import classnames from "classnames";
import _ from "lodash";

import { getDiscussions } from "./actions";

import Button from "../../Components/Button";
import FeedBox from "../../Components/FeedBox";
import SideBar from "../../Components/SideBar";

import appLayout from "../../SharedStyles/appLayout.module.css";
import styles from "./styles.module.css";

class ForumFeed extends Component {
  componentDidMount() {
    const { currentForumId, getDiscussions } = this.props;
    getDiscussions(currentForumId());
  }

  componentDidUpdate(prevProps) {
    const { currentForum, currentForumId, getDiscussions } = this.props;

    // get the discussions again
    // if the forum didn't matched
    if (prevProps.currentForum !== currentForum) {
      const feedChanged = true;
      getDiscussions(currentForumId(), feedChanged);
    }
  }

  handleSortingChange(newSortingMethod) {
    const { currentForum, getDiscussions, sortingMethod } = this.props;

    if (sortingMethod !== newSortingMethod) {
      getDiscussions(currentForum, false, true);
    }
  }

  renderNewDiscussionButtion() {
    const { currentForum } = this.props;
    if (window.innerWidth < 800) {
      return (
        <div
          className={classnames(
            appLayout.showOnMediumBP,
            styles.newDiscussionBtn
          )}
        >
          <Link to={`/new_discussion/${currentForum}`}>
            <Button type="outline" fullWidth noUppercase>
              New Discussion
            </Button>
          </Link>
        </div>
      );
    }
  }

  render() {
    const {
      currentForum,
      discussions,
      fetchingDiscussions,
      sortingMethod,
      error,
    } = this.props;

    if (error) {
      return <div className={classnames(styles.errorMsg)}>{error}</div>;
    }

    return (
      <div
        className={classnames(appLayout.constraintWidth, styles.contentArea)}
      >
        <Helmet>
          <title>{`ReForum | ${currentForum}`}</title>
        </Helmet>

        <div className={appLayout.primaryContent}>
          <FeedBox
            type="general"
            loading={fetchingDiscussions}
            discussions={discussions}
            currentForum={currentForum}
            onChangeSortingMethod={this.handleSortingChange.bind(this)}
            activeSortingMethod={sortingMethod}
          />

          {this.renderNewDiscussionButtion()}
        </div>

        <div className={appLayout.secondaryContent}>
          <SideBar currentForum={currentForum} />
        </div>
      </div>
    );
  }
}

export default connect(
  (state) => {
    return {
      currentForum: state.app.currentForum,
      currentForumId: () => {
        const currentForumObj = _.find(state.app.forums, {
          forum_slug: state.app.currentForum,
        });
        if (currentForumObj) return currentForumObj._id;
        else return null;
      },
      fetchingDiscussions: state.feed.fetchingDiscussions,
      discussions: state.feed.discussions,
      sortingMethod: state.feed.sortingMethod,
      error: state.feed.error,
    };
  },
  (dispatch) => {
    return {
      getDiscussions: (
        currentForumId,
        feedChanged,
        sortingMethod,
        sortingChanged
      ) => {
        dispatch(
          getDiscussions(
            currentForumId,
            feedChanged,
            sortingMethod,
            sortingChanged
          )
        );
      },
    };
  }
)(ForumFeed);
