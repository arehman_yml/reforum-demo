import { getData } from '../../Utils/axios';

/**
 * feed apis
 */
export const fetchDiscussions = (forum_id, sortingMethod) => {
  return getData(`/api/forum/${forum_id}/discussions`);
};
