import {
  START_FETCHING_DISCUSSIONS,
  FETCHING_DISCUSSIONS_SUCCESS,
  FETCHING_DISCUSSIONS_FAILURE,
  INVALID_FORUM,
} from "./constants";
import { fetchDiscussions } from "./api";

/**
 * action to fetch forum discussions
 * @param  {String}  forum               current forum slug
 * @param  {Boolean} feedChanged         if the feed has been changed, default is false
 * @param  {String}  sortingMethod       define the sorting method, default is 'date'
 * @param  {Boolean} sortingChanged      if user chagned the sorting method
 * @return {thunk}
 */
export const getDiscussions = (
  forumId,
  feedChanged = false,
  sortingChanged = false
) => {
  return (dispatch, getState) => {
    const sortingMethod = getState().feed.sortingMethod;

    // show the loading message when user change forum or change sorting method
    if (feedChanged || sortingChanged)
      dispatch({ type: START_FETCHING_DISCUSSIONS });

    if (!forumId) {
      dispatch({ type: INVALID_FORUM });
    } else {
      // start fetching discussions
      fetchDiscussions(forumId, sortingMethod).then(
        (data) =>
          dispatch({ type: FETCHING_DISCUSSIONS_SUCCESS, payload: data.data }),
        (error) => dispatch({ type: FETCHING_DISCUSSIONS_FAILURE })
      );
    }
  };
};
