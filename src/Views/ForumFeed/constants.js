export const START_FETCHING_DISCUSSIONS = 'start_fetching_discussions';
export const STOP_FETCHING_DISCUSSIONS = 'stop_fetching_discussions';
export const FETCHING_DISCUSSIONS_SUCCESS = 'fetching_discussions_success';
export const FETCHING_DISCUSSIONS_FAILURE = 'fetching_discussions_failure';

export const INVALID_FORUM = 'invalid_forum';
