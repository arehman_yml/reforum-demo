import { getData, putData, postData, deleteData } from '../../Utils/axios';

/**
 * single discussion apis
 */
export const fetchSingleDiscussion = (discussion_slug) => {
  return getData(`/api/discussion/${discussion_slug}`);
};

export const toggleFavoriteApi = (discussion_id) => {
  return putData(`/api/discussion/toggleFavorite/${discussion_id}`);
};

export const postOpinionApi = (opinion) => {
  return postData('/api/opinion/newOpinion', opinion);
};

export const deletePostApi = (discussionSlug) => {
  return deleteData(`/api/discussion/deleteDiscussion/${discussionSlug}`);
};

export const deleteOpinionApi = (opinionId) => {
  return deleteData(`/api/opinion/deleteOpinion/${opinionId}`);
};
