import React, { Component } from "react";
import { withRouter } from "react-router";
import cx from "classnames";
import { connect } from "react-redux";
import { login } from "../../App/actions";

import styles from "./styles.module.css";

class Login extends Component {
  state = {
    email: "",
    password: "",
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.props.doLogin(this.state);
  };

  goToSignup = () => {
    this.props.history.push("/auth/signup");
  };

  onChange = (e) => this.setState({ [e.target.id]: e.target.value });

  render() {
    const { email, password } = this.state;
    const activeBtnCss = cx([styles.HeaderButton, styles.active]);
    return (
      <main className={styles.loginContainer}>
        <div className={styles.header}>
          <button className={styles.HeaderButton} onClick={this.goToSignup}>
            Sign up
          </button>
          <button className={activeBtnCss}>Log in</button>
        </div>
        <form
          name="login-form"
          className={styles.loginForm}
          onSubmit={this.onSubmit}
        >
          <label htmlFor="email">Email</label>
          <input
            id="email"
            type="text"
            value={email}
            onChange={this.onChange}
          />

          <label htmlFor="password">Password</label>
          <input
            id="password"
            type="password"
            value={password}
            onChange={this.onChange}
          />

          <a href="/signup" className={styles.loginLink}>
            Create an account
          </a>

          <button type="submit" className={styles.submitBtn}>
            Login
          </button>
        </form>
      </main>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  doLogin: (data) => dispatch(login(data)),
});

export default withRouter(connect(null, mapDispatchToProps)(Login));
