import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";

// app store
import appStore from "./store";

// app views
import ForumFeed from "../Views/ForumFeed";
import SingleDiscussion from "../Views/SingleDiscussion";
import NewDiscussion from "../Views/NewDiscussion";
import SignUp from "../Views/SignUp";
import Login from "../Views/Login";
import NotFound from "../Views/NotFound";
import PrivateRoute from "../Containers/PrivateRoute";
import PublicRoute from "../Containers/PublicRoute";

import "../SharedStyles/appLayout.module.css";
import "../SharedStyles/globalStyles.module.css";

export default () => (
  <Provider store={appStore}>
    <BrowserRouter>
      <Switch>
        <PrivateRoute
          path="/discussion/:forum/:discussion"
          exact
          component={SingleDiscussion}
        />
        <PrivateRoute path="/forum/:forum" exact component={ForumFeed} />
        <PrivateRoute
          path="/new_discussion/:forum/"
          exact
          component={NewDiscussion}
        />
        <PrivateRoute path="/" exact component={ForumFeed} />
        <PublicRoute path="/auth/signup" component={SignUp} />
        <PublicRoute path="/auth/login" component={Login} />
        <Route path="*" component={NotFound} />
      </Switch>
    </BrowserRouter>
  </Provider>
);
