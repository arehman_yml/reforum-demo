import { getData, postData } from "../Utils/axios";

export const fetchForums = () => {
  return getData("/api/forum");
};

export const fetchUser = () => {
  return getData("/api/user/getUser");
};

export const signUpApi = (userData) => {
  return postData("/api/user/signup", userData);
};

export const loginApi = (userData) => {
  return postData("/api/user/login", userData);
};

export const logoutApi = () => {
  return postData("/api/user/logout");
};
