import React, { Component } from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { Helmet } from "react-helmet";

import Header from "./../Containers/Header";
import Footer from "./../Components/Footer";
import styles from "./styles.module.css";

import { getForums, updateCurrentForum, getUser } from "./actions";

class AppContainer extends Component {
  componentDidMount() {
    const { match, updateCurrentForum, getForums, getUser } = this.props;

    // get all forum list
    getForums();

    // check for authenticated user
    getUser();

    // set current forum based on route
    const currentForum = match.params.forum || "";
    updateCurrentForum(currentForum);
  }

  componentDidUpdate() {
    const {
      forums,
      match: { params },
      currentForum,
      updateCurrentForum,
    } = this.props;

    let newCurrentForum = "";
    if (params.forum) newCurrentForum = params.forum;
    else if (forums) newCurrentForum = forums[0].forum_slug;

    // update current forum if necessery
    if (newCurrentForum !== currentForum) updateCurrentForum(newCurrentForum);
  }

  render() {
    const { forums } = this.props;

    // render only if we get the forum lists
    if (forums) {
      return (
        <div>
          <Helmet>
            <title>ReForum</title>
          </Helmet>

          <Header />
          {this.props.children}
          <Footer />
        </div>
      );
    }

    return <div className={styles.loadingWrapper}>Loading...</div>;
  }
}

const mapStateToProps = (state) => ({
  forums: state.app.forums,
  currentForum: state.app.currentForum,
});

const mapDispatchToProps = (dispatch) => ({
  getForums: () => dispatch(getForums()),
  updateCurrentForum: (currentForum) =>
    dispatch(updateCurrentForum(currentForum)),
  getUser: () => dispatch(getUser()),
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(AppContainer)
);
