import React, { Component } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import styles from "./styles.module.css";

import closeIcon from "../../Assets/close.svg";

class Tag extends Component {
  render() {
    const { name, withRemove, removeAction } = this.props;

    return (
      <div
        className={classnames(styles.tag, withRemove && styles.tagWithRemove)}
      >
        <span className={styles.taglabel}>{name}</span>
        {withRemove && (
          <img
            src={closeIcon}
            alt="Remove tag"
            onClick={removeAction}
            className={styles.tagIcon}
            width="14"
            height="14"
          />
        )}
      </div>
    );
  }
}

Tag.defaultProps = {
  name: "",
  withRemove: false,
  removeAction: () => {},
};

Tag.propTypes = {
  name: PropTypes.string.isRequired,
  withRemove: PropTypes.bool,
  removeAction: PropTypes.func,
};

export default Tag;
