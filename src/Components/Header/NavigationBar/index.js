import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter, Link } from "react-router-dom";
import _ from "lodash";
import cx from "classnames";
import styles from "./styles.module.css";

class NavigationBar extends Component {
  render() {
    const { navigationLinks, match } = this.props;

    if (navigationLinks) {
      return (
        <ul className={styles.navigationBar}>
          {navigationLinks.map((link) => {
            const activeForum = match.params.forum;
            const isActive = link.name.toLowerCase().startsWith(activeForum);
            const css = cx([styles.links, isActive ? styles.linkActive : ""]);
            return (
              <li key={_.uniqueId("navLink_")}>
                <Link className={css} to={link.link}>
                  {link.name}
                </Link>
              </li>
            );
          })}
        </ul>
      );
    }

    return null;
  }
}

NavigationBar.defaultProps = {
  navigationLinks: [],
};

NavigationBar.propTypes = {
  navigationLinks: PropTypes.array,
};

export default withRouter(NavigationBar);
