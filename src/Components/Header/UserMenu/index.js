import React, { Component } from "react";
import PropTypes from "prop-types";
import classnames from "classnames";
import { withRouter } from "react-router";

import styles from "./styles.module.css";
import Button from "./../../../Components/Button";
import avatar from "../../../Assets/avatar.png";

class UserMenu extends Component {
  constructor(props) {
    super(props);
    this.state = { activeSubMenu: false };
    this.toggleSubMenu = this.toggleSubMenu.bind(this);
  }

  handleClickOutside() {
    this.setState({ activeSubMenu: false });
  }

  toggleSubMenu() {
    this.setState((prevState) => {
      return { activeSubMenu: !prevState.activeSubMenu };
    });
  }

  renderSubMenu() {
    const { activeSubMenu } = this.state;
    const { signedIn, logout } = this.props;

    if (activeSubMenu) {
      return (
        <div className={styles.subMenu}>
          <Button
            className={styles.subMenuClose}
            onClick={this.toggleSubMenu}
            alwaysActive
          >
            <i className={classnames("fa fa-close")}></i>
          </Button>

          {signedIn && (
            <span role="button" className={styles.subMenuItem} onClick={logout}>
              Sign Out
            </span>
          )}
        </div>
      );
    }

    return null;
  }

  navigateToOnboarding = () => {
    this.props.history.push("/auth/signup");
  };

  render() {
    const { signedIn, fullName } = this.props;

    if (signedIn) {
      return (
        <div style={{ position: "relative" }}>
          <div className={styles.container} onClick={this.toggleSubMenu}>
            <img
              className={styles.userAvatar}
              src={avatar}
              alt={`${fullName} Avatar`}
            />
            <span className={styles.title}>{fullName}</span>
          </div>
          {this.renderSubMenu()}
        </div>
      );
    }

    return (
      <div className={styles.container}>
        <Button
          alwaysActive
          className={classnames(styles.signInBtn, styles.title)}
          onClick={this.navigateToOnboarding}
        >
          Sign Up / Sign In
        </Button>
      </div>
    );
  }
}

UserMenu.defaultProps = {
  signedIn: false,
  fullName: "",
  gitHandler: "",
  avatar: "",
};

UserMenu.propTypes = {
  signedIn: PropTypes.bool.isRequired,
  fullName: PropTypes.string,
  gitHandler: PropTypes.string,
  avatar: PropTypes.string,
  logout: PropTypes.func.isRequired,
};

export default withRouter(UserMenu);
