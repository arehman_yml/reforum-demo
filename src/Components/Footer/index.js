import React from "react";
import classnames from "classnames";

import styles from "./styles.module.css";
import appLayout from "../../SharedStyles/appLayout.module.css";

const Footer = () => {
  const css = classnames(appLayout.constraintWidth, styles.contentArea);
  return <div className={css}>A forum built using React, Redux & Nodejs</div>;
};

export default Footer;
