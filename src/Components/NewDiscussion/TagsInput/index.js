import React, { Component } from "react";
import PropTypes from "prop-types";
import styles from "./styles.module.css";

import Tag from "../../Tag";
import plusIcon from "../../../Assets/plus.svg";

class TagsInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      errorMsg: null,
      tags: [],
      tagName: "",
    };
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { value } = nextProps;
    this.setState({ tags: value, errorMsg: null });
  }

  validateTag(tagName) {
    const regex = /^[a-z0-9.\-_$@*!]{4,20}$/;
    return regex.test(tagName);
  }

  sameTag(tagName) {
    const { tags } = this.state;
    return tags.some((tag) => tag === tagName);
  }

  addTag() {
    const { tagName, tags } = this.state;

    if (this.validateTag(tagName)) {
      if (!this.sameTag(tagName)) {
        const newTags = tags.concat(tagName);
        this.setState({
          tags: newTags,
          errorMsg: null,
          tagName: "",
        });
        this.props.onChange(newTags);
      } else {
        this.setState({ errorMsg: "Same tag!!!" });
      }
    } else {
      this.setState({
        errorMsg:
          "Tags can only contain small letters and numbers. No space or special characters please. Min 4 and max 20 chars.",
      });
    }
  }

  removeTag(position) {
    const { tags } = this.state;
    const newTags = [
      ...tags.slice(0, position),
      ...tags.slice(position + 1, tags.length),
    ];
    this.setState({ tags: newTags });
    this.props.onChange(newTags);
  }

  renderTags() {
    const { tags } = this.state;

    return tags.map((tag, i) => {
      return (
        <Tag
          name={tag}
          key={tag}
          withRemove
          removeAction={() => {
            this.removeTag(i);
          }}
        />
      );
    });
  }

  renderInput() {
    const { tagName, tags } = this.state;
    const { maxTagCount } = this.props;

    if (tags.length < maxTagCount) {
      return (
        <div className={styles.inputContainer}>
          <input
            className={styles.tagInput}
            placeholder={"tag name..."}
            value={tagName}
            onChange={(e) => {
              this.setState({ tagName: e.target.value });
            }}
          />
          <img
            src={plusIcon}
            alt="add tag"
            width="14"
            height="14"
            onClick={() => this.addTag()}
            style={{ cursor: "pointer" }}
            role="button"
          />
        </div>
      );
    }

    return null;
  }

  render() {
    const { errorMsg } = this.state;

    return (
      <div className={styles.container}>
        <div className={styles.tagContainer}>
          <div className={styles.label}>Tags :</div>
          {this.renderTags()}
          {this.renderInput()}
        </div>
        {errorMsg && <div className={styles.errorMsg}>{errorMsg}</div>}
      </div>
    );
  }
}

TagsInput.defaultProps = {
  value: [],
  maxTagCount: 3,
  onChange: () => {},
};

TagsInput.propTypes = {
  value: PropTypes.array,
  maxTagCount: PropTypes.number,
  onChange: PropTypes.func,
};

export default TagsInput;
