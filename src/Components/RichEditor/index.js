import React, { Component } from "react";
import PropTypes from "prop-types";

import classnames from "classnames";
import styles from "./styles.module.css";

import Button from "../Button";

class RichEditor extends Component {
  state = {
    value: "",
  };

  componentDidMount() {
    const { value } = this.props;
    if (value) {
      this.setState({ value });
    }
  }

  onEditorUpdated = (e) => {
    this.setState({ value: e.target.value });
    this.props.onChange(e.target.value);
  };

  render() {
    const { type, onSave, readOnly } = this.props;

    let saveButtonLabel = "";
    if (type === "newOpinion") saveButtonLabel = "Reply";
    if (type === "newDiscussion") saveButtonLabel = "Post Discussion";

    let placeholder = "";
    if (type === "newOpinion") placeholder = "Your opinion...";
    if (type === "newDiscussion") placeholder = "Discussion summary...";

    return (
      <div
        className={classnames(
          styles.container,
          readOnly && styles.readOnlyContainer
        )}
      >
        <textarea
          readOnly={readOnly}
          placeholder={placeholder}
          className={classnames(
            styles.editorContainer,
            !readOnly && styles[type],
            readOnly && styles.readOnlyEditorContainer
          )}
          value={this.state.value}
          onChange={this.onEditorUpdated}
        ></textarea>

        {!readOnly && (
          <Button noUppercase style={{ alignSelf: "center" }} onClick={onSave}>
            {saveButtonLabel}
          </Button>
        )}
      </div>
    );
  }
}

RichEditor.defaultProps = {
  readOnly: false,
  value: null,
  type: "newDiscussion",
  onChange: () => {},
  onSave: () => {},
};

RichEditor.propTypes = {
  readOnly: PropTypes.bool,
  value: PropTypes.any,
  type: PropTypes.oneOf(["newDiscussion", "newOpinion"]),
  onChange: PropTypes.func,
  onSave: PropTypes.func,
};

export default RichEditor;
