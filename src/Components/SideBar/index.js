import React, { Component } from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router";
import styles from "./styles.module.css";

import Button from "../../Components/Button";

class SideBar extends Component {
  render() {
    const { currentForum, history } = this.props;
    const newDiscussionUrl = `/new_discussion/${currentForum}/`;
    return (
      <div className={styles.sidebarContainer}>
        <Button
          type="outline"
          fullWidth
          noUppercase
          onClick={() => history.push(newDiscussionUrl)}
        >
          New Discussion
        </Button>
      </div>
    );
  }
}

SideBar.defaultProps = {
  currentForum: "general",
};

SideBar.propTypes = {
  currentForum: PropTypes.string,
};

export default withRouter(SideBar);
