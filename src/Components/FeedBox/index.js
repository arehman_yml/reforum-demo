import React, { Component } from "react";
import PropTypes from "prop-types";
import styles from "./styles.module.css";

import DiscussionBox from "./DiscussionBox";

class FeedBox extends Component {
  renderEmptyDiscussionLine(loading, discussions) {
    if (!loading) {
      if (!discussions || discussions.length === 0) {
        return <div className={styles.loading}>No discussions...</div>;
      }
    }
  }

  render() {
    const {
      type,
      loading,
      discussions,
      currentForum,
      userProfile,
    } = this.props;

    let discussionBoxTitle = "";
    if (type === "general") discussionBoxTitle = "Discussions";

    return (
      <div className={styles.container}>
        <div className={styles.header}>
          <span className={styles.title}>{discussionBoxTitle}</span>
        </div>
        {loading && <div className={styles.loading}>Loading...</div>}
        {this.renderEmptyDiscussionLine(loading, discussions)}
        {!loading && (
          <div className={styles.discussions}>
            {discussions &&
              discussions.map((discussion) => (
                <DiscussionBox
                  userProfile={userProfile}
                  key={discussion._id}
                  userName={discussion.user.fullName}
                  userGitHandler={discussion.user.userName}
                  discussionTitle={discussion.title}
                  time={discussion.date}
                  tags={discussion.tags}
                  opinionCount={discussion.opinion_count}
                  voteCount={discussion.favorites.length}
                  link={`/discussion/${
                    userProfile ? discussion.forum.forum_slug : currentForum
                  }/${discussion.discussion_slug}`}
                />
              ))}
          </div>
        )}
      </div>
    );
  }
}

FeedBox.defaultProps = {
  type: "general",
  loading: false,
  discussions: [],
  currentForum: "general",
  activeSortingMethod: "date",
  onChangeSortingMethod: (val) => {},
  userProfile: false,
};

FeedBox.propTypes = {
  type: PropTypes.oneOf(["general"]),
  loading: PropTypes.bool,
  discussions: PropTypes.array,
  currentForum: PropTypes.string,
  activeSortingMethod: PropTypes.string,
  onChangeSortingMethod: PropTypes.func,
  userProfile: PropTypes.bool,
};

export default FeedBox;
