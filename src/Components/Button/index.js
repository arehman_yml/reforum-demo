import React, { Component } from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import styles from "./styles.module.css";

class Button extends Component {
  render() {
    const {
      type,
      fullWidth,
      noUppercase,
      className,
      style,
      onClick,
      alwaysActive,
    } = this.props;

    const css = cx(
      styles.button,
      styles.buttonDefaults,
      styles[type],
      fullWidth && styles.fullWidth,
      noUppercase && styles.noUppercase,
      alwaysActive && styles.alwaysActive,
      className
    );

    return (
      <button onClick={onClick} className={css} style={style}>
        {this.props.children}
      </button>
    );
  }
}

Button.defaultProps = {
  type: "default",
  fullWidth: false,
  noUppercase: false,
  alwaysActive: false,
  className: "",
  style: {},
  onClick: () => {},
};

Button.propTypes = {
  type: PropTypes.oneOf(["default", "outline"]),
  fullWidth: PropTypes.bool,
  noUppercase: PropTypes.bool,
  alwaysActive: PropTypes.bool,
  className: PropTypes.string,
  style: PropTypes.object,
  onClick: PropTypes.func,
};

export default Button;
