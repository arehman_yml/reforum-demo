import React, { Component } from "react";
import PropTypes from "prop-types";
import moment from "moment";
import classnames from "classnames";
import styles from "./styles.module.css";

import Button from "../../Button";
import RichEditor from "../../RichEditor";
import userAvatar from "../../../Assets/avatar.png";

class Opinion extends Component {
  render() {
    const {
      opinionId,
      opDate,
      opContent,
      userId,
      user,
      currentUserId,
      deleteAction,
      deletingOpinion,
    } = this.props;

    const { email, fullName } = user;

    let dateDisplay = moment(opDate);
    dateDisplay = dateDisplay.from(moment());

    const allowDelete = userId === currentUserId;

    return (
      <article className={styles.container}>
        <div className={styles.infoContainer}>
          <img className={styles.avatar} alt="avatar" src={userAvatar} />
          <div className={styles.userInfo}>
            <span className={styles.name}>{fullName}</span>
            <span className={styles.gitHandler}>&lt;{email}&gt;</span>
          </div>
          <div className={styles.dateInfo}>{dateDisplay}</div>
          {allowDelete && (
            <Button
              className={styles.deleteButton}
              noUppercase
              onClick={() => {
                deleteAction(opinionId);
              }}
            >
              <i className={classnames("fa fa-trash", styles.trashIcon)}></i>
              <span>Delete</span>
            </Button>
          )}
        </div>

        <div className={styles.opContent}>
          <RichEditor readOnly value={opContent} />
        </div>

        {deletingOpinion === opinionId && (
          <div className={styles.deletingOpinion}>Deleting Opinion ...</div>
        )}
      </article>
    );
  }
}

Opinion.defaultProps = {
  opinionId: "12345",
  opDate: "a day ago",
  opContent:
    "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
  userId: "12345",
  user: {},
  currentUserId: "12345",
  deleteAction: () => {},
  deletingOpinion: null,
};

Opinion.propTypes = {
  opinionId: PropTypes.string,
  opDate: PropTypes.any,
  opContent: PropTypes.string,
  userId: PropTypes.string,
  user: PropTypes.object,
  currentUserId: PropTypes.string,
  deleteAction: PropTypes.func,
  deletingOpinion: PropTypes.any,
};

export default Opinion;
