import React, { Component } from "react";
import PropTypes from "prop-types";

import _ from "lodash";
import moment from "moment";
import classnames from "classnames";
import styles from "./styles.module.css";

import userAvatar from "../../../Assets/avatar.png";
import Button from "../../Button";
import Tag from "../../Tag";
import RichEditor from "../../RichEditor";

class Discussion extends Component {
  render() {
    const {
      id,
      user,
      discTitle,
      discDate,
      discContent,
      tags,
      favoriteCount,
      favoriteAction,
      userFavorited,
      toggleingFavorite,
      allowDelete,
      deletingDiscussion,
      deleteAction,
    } = this.props;

    let dateDisplay = moment(discDate);
    dateDisplay = dateDisplay.from(moment());

    let favCount = "";
    if (toggleingFavorite) favCount = "Toggling Favorite...";
    else if (userFavorited) favCount = favoriteCount;
    else if (favoriteCount === 0) favCount = "Make favorite";
    else if (favoriteCount === 1) favCount = "1 favorite";
    else favCount = `${favoriteCount} favorites`;

    return (
      <div className={styles.container}>
        <div className={styles.infoContainer}>
          <img className={styles.avatar} src={userAvatar} alt="Avatar" />
          <div className={styles.columnOnSmallDP}>
            <div className={styles.userInfo}>
              <span className={styles.name}>{user.fullName}</span>
              <span className={styles.gitHandler}>&lt;{user.email}&gt;</span>
            </div>
            <div className={styles.dateInfo}>{dateDisplay}</div>
          </div>
        </div>

        <div className={styles.discTitle}>{discTitle}</div>
        <div className={styles.discContent}>
          <RichEditor readOnly={true} value={discContent} />
        </div>

        <div className={styles.discFooter}>
          <div className={styles.tags}>
            {tags.map((tag) => (
              <Tag name={tag} key={_.uniqueId("tag_")} />
            ))}
          </div>
          <Button
            noUppercase
            className={styles.favoriteButton}
            onClick={() => {
              !toggleingFavorite && favoriteAction(id);
            }}
          >
            <i
              className={classnames(
                `fa fa-${userFavorited ? "heart" : "heart-o"}`
              )}
            ></i>
            <span>{favCount}</span>
          </Button>

          {allowDelete && (
            <Button
              noUppercase
              className={styles.deleteButton}
              onClick={() => {
                deleteAction();
              }}
            >
              <i className={classnames("fa fa-trash", styles.trashIcon)}></i>
              <span>Delete</span>
            </Button>
          )}
        </div>

        {deletingDiscussion && (
          <div className={styles.deletingDiscussion}>
            Deleting Discussion...
          </div>
        )}
      </div>
    );
  }
}

Discussion.defaultProps = {
  id: 0,
  userAvatar,
  user: {},
  discTitle: "Default Discussion Title",
  discDate: "a day ago",
  discContent:
    "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
  tags: ["react", "redux", "webkit"],
  favoriteCount: 1,
  favoriteAction: () => {},
  userFavorited: false,
  toggleingFavorite: false,
  allowDelete: false,
  deletingDiscussion: false,
  deleteAction: () => {},
};

Discussion.propTypes = {
  id: PropTypes.any,
  user: PropTypes.object,
  discTitle: PropTypes.string,
  discDate: PropTypes.any,
  discContent: PropTypes.any,
  tags: PropTypes.array,
  favoriteCount: PropTypes.number,
  favoriteAction: PropTypes.func,
  userFavorited: PropTypes.bool,
  toggleingFavorite: PropTypes.bool,
  allowDelete: PropTypes.bool,
  deletingDiscussion: PropTypes.bool,
  deleteAction: PropTypes.func,
};

export default Discussion;
