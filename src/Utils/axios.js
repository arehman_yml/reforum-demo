import axios from "axios";

const axiosInstance = axios.create({
  baseURL: process.env.REACT_APP_BACKEND_URL,
});

const responseHandler = (resp) => {
  return resp.status >= 200 && resp.status < 300
    ? Promise.resolve(resp)
    : Promise.reject(resp);
};

const headers = () => {
  const token = localStorage.getItem("token");
  if (!token) {
    return {};
  }

  return {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
};

const getData = (url) =>
  axiosInstance.get(url, { ...headers() }).then(responseHandler);

const postData = (url, data) =>
  axiosInstance.post(url, data, { ...headers() }).then(responseHandler);

const deleteData = (url, data) =>
  axiosInstance({ method: "delete", url, data, ...headers() });

const putData = (url, data) =>
  axiosInstance({ method: "put", url, data, ...headers() }).then(
    responseHandler
  );

export { axiosInstance as axios, getData, postData, deleteData, putData };
