import { convertNumberToCurrency, formatBytes } from "./utils";

describe("Tests for function: convertNumberToCurrency", () => {
  test("Case 1", () => {
    const input = 1213141;
    const expectedOutput = "$1,213,141";
    const output = convertNumberToCurrency(input);
    expect(output).toBe(expectedOutput);
  });

  test("Case 2", () => {
    const input = 0;
    const expectedOutput = "$0";
    const output = convertNumberToCurrency(input);
    expect(output).toBe(expectedOutput);
  });
});

describe("Tests for function: formatBytes", () => {
  test("Case 1", () => {
    const input = 1024;
    const expectedOutput = "1 KB";
    const output = formatBytes(input);
    expect(output).toBe(expectedOutput);
  });

  test("Case 2", () => {
    const input = 4013493;
    const expectedOutput = "3.83 MB";
    const output = formatBytes(input);
    expect(output).toBe(expectedOutput);
  });
});
