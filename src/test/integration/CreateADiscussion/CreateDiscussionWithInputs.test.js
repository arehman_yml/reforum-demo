import React from "react";
import MockAdapter from "axios-mock-adapter";
import {
  render,
  screen,
  fireEvent,
  waitForElementToBeRemoved,
  cleanup,
} from "@testing-library/react";
import App from "../../../App";
import { axios } from "../../../Utils/axios";
import forums from "../../mock-data/forums";
import currentUser from "../../mock-data/currentUser";
import newDiscussionResponse from "../../mock-data/newDiscussionResponse";
const mAxios = new MockAdapter(axios);

beforeEach(() => {
  window.localStorage.setItem("token", "some random token");
  render(<App />);

  mAxios.onGet("/api/forum").reply(200, forums);
  mAxios.onGet("/api/user/getUser").reply(200, currentUser);
  mAxios
    .onGet("/api/forum/591dd4a8734d1d38b5b749ae/discussions")
    .reply(200, []);
  mAxios
    .onPost("/api/discussion/newDiscussion")
    .reply(200, newDiscussionResponse);
});

afterEach(() => {
  window.localStorage.clear();
  mAxios.reset();
  cleanup();
});

const { queryByText, queryByPlaceholderText, queryByAltText } = screen;

describe("Tests for Creating a post", () => {
  /**
   * 1) Trying to create discussion without entering the title.
   * 2) trying to create without selecting the tag.
   * 3) Trying to create without entering discussion summary.
   * 4) Creating a discussion with all inputs.
   */

  it("should create a discussion with valid inputs", async (done) => {
    await waitForElementToBeRemoved(() => queryByText("Loading..."));
    fireEvent.click(queryByText("New Discussion"));

    const titleInput = queryByPlaceholderText("Discussion title...");
    fireEvent.change(titleInput, {
      target: { value: "My first automated test" },
    });

    const tagInput = queryByPlaceholderText("tag name...");
    fireEvent.change(tagInput, {
      target: { value: "general" },
    });
    const addTagBtn = queryByAltText("add tag");
    fireEvent.click(addTagBtn);

    const summaryInput = queryByPlaceholderText("Discussion summary...");
    fireEvent.change(summaryInput, { target: { value: "Hello all" } });

    const postBtn = queryByText("Post Discussion");
    fireEvent.click(postBtn);

    await waitForElementToBeRemoved(() => queryByText("Posting discussion..."));

    expect(queryByText("Your discussion is created :-)")).toBeInTheDocument();

    done();
  });
});
