import React from "react";
import MockAdapter from "axios-mock-adapter";
import {
  render,
  screen,
  fireEvent,
  waitForElementToBeRemoved,
  cleanup,
} from "@testing-library/react";
import App from "../../../App";
import { axios } from "../../../Utils/axios";
import forums from "../../mock-data/forums";
import currentUser from "../../mock-data/currentUser";
const mAxios = new MockAdapter(axios);

beforeEach(() => {
  window.localStorage.setItem("token", "some random token");
  render(<App />);

  mAxios.onGet("/api/forum").reply(200, forums);
  mAxios.onGet("/api/user/getUser").reply(200, currentUser);
  mAxios
    .onGet("/api/forum/591dd4a8734d1d38b5b749ae/discussions")
    .reply(200, []);
});

afterEach(() => {
  window.localStorage.clear();
  mAxios.reset();
  cleanup();
});

const { queryByText, queryByPlaceholderText } = screen;

describe("Tests for Creating a new post", () => {
  it("should not create a discussion without title", async (done) => {
    await waitForElementToBeRemoved(() => screen.queryByText("Loading..."));

    fireEvent.click(queryByText("New Discussion"));
    const titleInput = queryByPlaceholderText("Discussion title...");
    fireEvent.change(titleInput, {
      target: { value: "My" },
    });
    fireEvent.click(queryByText("Post Discussion"));

    expect(
      queryByText("Title should be at least 15 characters.")
    ).toBeInTheDocument();
    done();
  });
});
