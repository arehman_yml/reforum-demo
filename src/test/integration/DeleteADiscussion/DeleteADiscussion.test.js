import React from "react";
import MockAdapter from "axios-mock-adapter";
import {
  render,
  screen,
  fireEvent,
  waitForElementToBeRemoved,
  cleanup,
} from "@testing-library/react";
import App from "../../../App";
import { axios } from "../../../Utils/axios";
import forums from "../../mock-data/forums";
import currentUser from "../../mock-data/currentUser";
import discussions from "../../mock-data/discussions";
import discussionDetail from "../../mock-data/discussionDetail";
const mAxios = new MockAdapter(axios);

beforeEach(() => {
  window.localStorage.setItem("token", "some random token");
  render(<App />);

  mAxios.onGet("/api/forum").reply(200, forums);
  mAxios.onGet("/api/user/getUser").reply(200, currentUser);
  mAxios
    .onGet("/api/forum/591dd4a8734d1d38b5b749ae/discussions")
    .reply(200, discussions);
  ////// Step 3: Mock the response for loading the discussion detail page (use ./mock-data/discussionDetail)
});

afterEach(() => {
  window.localStorage.clear();
  mAxios.reset();
  cleanup();
});

const { queryByText, queryByPlaceholderText, queryByAltText } = screen;

describe("Tests for delete a discussion", () => {
  it("deletes a discussion", async (done) => {
    // screen.debug();
    //////Step 1: wait for the "Loading..."" text to disappear

    //////Step 2: trigger a click event on one of the discussion based on its title

    //////Step 4: Wait for the discussion detail page to load

    //////Step 5: Trigger a click event on delete button

    //////Step 6: Wait for the page to load

    //////Step 7: Ensure the discussion's title is not found
    done();
  });
});
