export default [
  {
    _id: "591dd4a8734d1d38b5b749ae",
    forum_slug: "general",
    forum_name: "General",
  },
  {
    _id: "591dd4c6734d1d38b5b749b6",
    forum_slug: "react",
    forum_name: "React",
  },
  {
    _id: "593464e615b6410004f59654",
    forum_slug: "redux",
    forum_name: "Redux",
    __v: 0,
  },
  {
    _id: "593464fe15b6410004f59655",
    forum_slug: "express",
    forum_name: "ExpressJS",
    __v: 0,
  },
];
