import React, { Component } from "react";
import { connect } from "react-redux";
import classnames from "classnames";

import { logout } from "../../App/actions";
import appLayout from "../../SharedStyles/appLayout.module.css";
import styles from "./styles.module.css";

// components for Header
import UserMenu from "../../Components/Header/UserMenu";
import Logo from "../../Components/Header/Logo";
import NavigationBar from "../../Components/Header/NavigationBar";

class Header extends Component {
  renderNavLinks() {
    const { forums } = this.props;

    if (forums) {
      return forums.map((forum) => {
        return {
          id: forum._id,
          name: forum.forum_name,
          link: `/forum/${forum.forum_slug}`,
        };
      });
    }

    return null;
  }

  render() {
    const { authenticated, fullName, userName, avatarUrl } = this.props.user;

    return (
      <div className={classnames(appLayout.constraintWidth)}>
        <div className={styles.headerTop}>
          <Logo />
          <UserMenu
            signedIn={authenticated}
            fullName={fullName}
            gitHandler={userName}
            avatar={avatarUrl}
            logout={this.props.logout}
          />
        </div>
        {authenticated && (
          <NavigationBar navigationLinks={this.renderNavLinks()} />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.user,
  forums: state.app.forums,
  authenticated: state.user.authenticated,
});

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logout()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
