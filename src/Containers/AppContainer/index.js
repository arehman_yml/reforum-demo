import React, { Component } from "react";
import { Route } from "react-router-dom";

import App from "../../App/App";

export class AppContainer extends Component {
  render() {
    const { component: Child, ...rest } = this.props;
    return (
      <Route
        {...rest}
        render={(matchProps) => (
          <App {...matchProps}>
            <Child {...matchProps} />
          </App>
        )}
      ></Route>
    );
  }
}

export default AppContainer;
