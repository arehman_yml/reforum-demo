import React, { Component } from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import App from "../../App/App";

export class PrivateRoute extends Component {
  render() {
    const { component: Child, authenticated, ...rest } = this.props;
    if (!authenticated) {
      return <Redirect to="/auth/login" />;
    }

    return (
      <Route
        {...rest}
        render={(matchProps) => (
          <App {...matchProps}>
            <Child {...matchProps} />
          </App>
        )}
      ></Route>
    );
  }
}

const mapStateToProps = (state) => ({
  authenticated: state.user.authenticated,
});

export default connect(mapStateToProps)(PrivateRoute);
